﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            /// have conflict
            double score = 0;
            MlModel model = new CosineSimilarity();
            var key = "";
            Console.WriteLine("----------To quite chat, Please type Exit and hit enter-----------------");
        /// new comment
            var welcome = "";
            if (DateTime.UtcNow.AddHours(5.5).Hour < 12)
            {
                welcome = "Good Morning";
            }
            else if (DateTime.UtcNow.AddHours(5.5).Hour < 17)
            {
                welcome = "Good Afternoon";
            }
            else
            {
                welcome = "Good Evening";
            }

            // Sourov
            Console.WriteLine("Good night");
            Console.WriteLine(welcome+"! What is your name ?");
            var name = Console.ReadLine().Split(' ').LastOrDefault();
            Console.WriteLine(string.Format("Thank you {0}, How can i help you?", name));



            do
            {
                Console.Write(name + " : ");
                key = Console.ReadLine();
                key = key.ToLower();
                key = key.Replace("my", name).Replace("i am", name).Replace("me", name).Replace("i'm", name);
                model.Fit();
                var ans = model.Predict(key, out score);
                if (score >= 20)
                {
                    Console.WriteLine("EMPRESS BOT ["+score+"] : " + ans.Answer);
                }
                else if (key.ToLower() != "exit")
                {
                    Console.WriteLine("EMPRESS BOT : Sorry i cant understand your question, Do you please ask me more specific ?");
                }

            } while (key.ToLower() != "exit");
            Console.ReadKey();
        }

    }
}
