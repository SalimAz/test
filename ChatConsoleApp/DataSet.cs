﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;
using Azolution.Utility;

namespace ChatConsoleApp
{
    public class DataSet
    {
        //Foyaz Commit
        public DataSet()
        {
            //StopWords=new List<string>()
            //{"i","me","my","myself","we","our","ours","ourselves","you","you''re","you''ve","you''ll","you''d","your","yours","yourself","yourselves","he","him",
            // "his","himself","she","she''s","her","hers","herself","it","it''s","its","itself","they","them","their","theirs","themselves","what","which","who","whom","this",
            // "that","that''ll","these","those","am","is","are","was","were","be","been","being","have","has","had","having","do","does","did","doing","a","an","the","and","but",
            // "if","or","because","as","until","while","of","at","by","for","with","about","against","between","into","through","during","before","after","above","below","to",
            // "from","up","down","in","out","on","off","over","under","again","further","then","once","here","there","when","where","why","how","all","any","both","each","few",
            // "more","most","other","some","such","no","nor","not","only","own","same","so","than","too","very","s","t","can","will","just","don","don''t","should","should''ve",
            // "now","d","ll","m","o","re","ve","y","ain","aren","aren''t","couldn","couldn''t","didn","didn''t","doesn","doesn''t","hadn","hadn''t","hasn","hasn''t","haven","haven''t",
            // "isn","isn''t","ma","mightn","mightn''t","mustn","mustn''t","needn","needn''t","shan","shan''t","shouldn","shouldn''t","wasn","wasn''t","weren","weren''t","won",
            // "won''t","wouldn","wouldn''t"
            //};
        }
        public List<string> StopWords =new List<string>() ;
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }

        public readonly char[] Separator = new char[] { ' ', '.', ',', ':', ';' };
        public List<string> Words=new List<string>(); 
        

        //public double[,] GetContent(IEnumerable<DataSet> dataSets,string question )
        //{
        //    var documents = new List<DataSet>();
        //    var bow = string.Empty;
            
        //    foreach (var dataSet in dataSets)
        //    {
        //        var doc = dataSet.Question.EscChar().ToLower().Trim();
        //        dataSet.Words = doc.Split(Separator).ToList().ToList();
        //        documents.Add(dataSet);
        //        bow += doc +' ';
        //    }

        //    var ques = new DataSet();
        //    question = question.EscChar().ToLower().Trim();
        //    ques.Words = question.ToLower().Split(Separator).ToList();
        //    documents.Add(ques);
        //    bow +=question;

        //    var unicalWords = bow.Split(' ').GroupBy(g => g)
        //            .Select(s => s.Key).Distinct()
        //            .ToList();
        //    var uni = unicalWords.Distinct();
        //    var array = GetWeights(documents, uni.ToList());
        //    return array;
        //}

        //public double[,] GetWeights(List<DataSet> splitedDocuments, List<string> unicalWords)
        //{

        //    var uniDic = new Dictionary<int, string>();
        //    for (int i = 0; i < unicalWords.Count; i++)
        //    {
        //        uniDic.Add(i,unicalWords[i]);
        //    }

        //    double[,] matrix = new double[unicalWords.Count(), splitedDocuments.Count];

        //    for (int i = 0; i < splitedDocuments.Count; i++)
        //    {
        //        for (int j = 0; j < unicalWords.Count(); j++)
        //        {
        //            var key = unicalWords[j];
        //            var value = splitedDocuments[i].Words.Count(w => w == key);
        //            matrix[j, i] = value;
        //        }
        //    }
        //    return matrix;
        //}

        //public List<DataSet> GetTrainDataSet()
        //{
        //    var sql = "select * from ChatQA";
        //    var datasets = Data<DataSet>.DataSource(sql);
        //    return datasets;
        //}
    }
}
