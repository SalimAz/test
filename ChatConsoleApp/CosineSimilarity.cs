﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azolution.Utility;

namespace ChatConsoleApp
{
    public class CosineSimilarity : MlModel
    {
        //check
        // Hello 
        static double[,] _matrix;
        static List<string> _unicaList = new List<string>();
        public override void Fit()
        {
            if (_unicaList.Any()) return;
            var dataset = GetTrainDataSet();
            GetContent(dataset);
        }

        private List<DataSet> GetTrainDataSet()
        {
            try
            {
                var sql = "select * from ChatQA";
                var datasets = Data<DataSet>.DataSource(sql);
                return datasets;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void GetContent(IEnumerable<DataSet> dataSets)
        {
            try
            {
                var data = new DataSet();
                var documents = new List<DataSet>();
                var bow = string.Empty;

                foreach (var dataSet in dataSets)
                {
                    var doc = dataSet.Question.EscChar().ToLower().Trim();
                    dataSet.Words = doc.Split(data.Separator).ToList().ToList();
                    documents.Add(dataSet);
                    bow += doc + ' ';
                }

                var unicalWords = bow.Split(' ').GroupBy(g => g)
                    .Select(s => s.Key).Distinct()
                    .ToList();
                _unicaList = unicalWords.Distinct().ToList();
                _matrix = GetWeights(documents, _unicaList);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "(Error during process content)");
            }
        }

        public double[,] GetWeights(List<DataSet> splitedDocuments, List<string> unicalWords)
        {

            try
            {
                double[,] matrix = new double[unicalWords.Count(), splitedDocuments.Count];

                for (int i = 0; i < splitedDocuments.Count; i++)
                {
                    for (int j = 0; j < unicalWords.Count(); j++)
                    {
                        var key = unicalWords[j];
                        var value = splitedDocuments[i].Words.Count(w => w == key);
                        matrix[j, i] = value;
                    }
                }
                return matrix;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "(Error during get wights)");
            }
        }


        #region Prediction

        public override DataSet Predict(string question, out double score)
        {
            try
            {
                var dataset = GetTrainDataSet();
                var documents = new List<DataSet>();
                var ques = new DataSet();
                question = question.EscChar().ToLower().Trim();
                ques.Words = question.ToLower().Split(new DataSet().Separator).ToList();
                documents.Add(ques);
                var qMatrix = GetWeights(documents, _unicaList);

                var length = _matrix.GetLength(0);

                var qVec = new double[length];
                var ansVec = new double[length];

                var learningMatrices = new List<LearningMatrix>();


                for (int i = 0; i < length; i++)
                {
                    qVec[i] = qMatrix[i, 0];
                }

                for (int j = 0; j < dataset.ToList().Count; j++)
                {
                    for (int i = 0; i < length; i++)
                    {
                        ansVec[i] = _matrix[i, j];
                    }
                    var cosSimilarity = CalculateCosineSimilarity(qVec, ansVec);
                    learningMatrices.Add(new LearningMatrix()
                    {
                        QuestionId = dataset[j].QuestionId,
                        Score = Math.Round(cosSimilarity * 100, 2)
                    });
                }
                var bestLearning = learningMatrices.OrderByDescending(s => s.Score).FirstOrDefault();
                score = bestLearning.Score;
                return dataset.FirstOrDefault(q => q.QuestionId == bestLearning.QuestionId);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "(Error during predict)");
            }
        }

        private double CalculateCosineSimilarity(double[] vecA, double[] vecB)
        {
            try
            {
                var dotProduct = DotProduct(vecA, vecB);
                var magnitudeOfA = Magnitude(vecA);
                var magnitudeOfB = Magnitude(vecB);
                return dotProduct / (magnitudeOfA * magnitudeOfB);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "(Error during caculate similarity)");
            }
        }

        private double DotProduct(double[] vecA, double[] vecB)
        {
            try
            {
                return vecA.Select((t, i) => (t*vecB[i])).Sum();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + "(Error during dot product)");
            }
        }

        private double Magnitude(double[] vector)
        {
            try
            {
                return Math.Sqrt(DotProduct(vector, vector));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message+ "(Error during caculate magintude)");
            }
        }

        #endregion


        ///
    }
}
