﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace ChatConsoleApp
{
    public abstract class MlModel
    {
        public abstract void Fit();
        public abstract DataSet Predict(string question, out double score);


    }
}
